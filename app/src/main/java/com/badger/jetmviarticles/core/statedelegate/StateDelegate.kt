package com.badger.jetmviarticles.core.statedelegate

import com.badger.jetmviarticles.core.base.IBaseViewAction
import com.badger.jetmviarticles.core.base.IBaseViewEvent
import com.badger.jetmviarticles.core.base.IBaseViewState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class StateDelegate<in E : IBaseViewEvent, S : IBaseViewState, out P : ReducibleState<S>, A : IBaseViewAction> constructor(
    private val scope: CoroutineScope,
    initialState: S,
    bindSources: suspend (ActionManager<A>) -> Flow<P> = { emptyFlow() },
    private val onAction: suspend (ActionManager<A>, E) -> Unit = { _, _ -> },
    private val onEvent: suspend (ActionManager<A>, E) -> Flow<P> = { _, _ -> emptyFlow() },
) : IStateDelegate<E, S, A> {

    private val _state: MutableStateFlow<S> = MutableStateFlow(initialState)
    override val state get() = _state.asStateFlow()

    private val _actions: MutableSharedFlow<A> = MutableSharedFlow()
    override val actions get() = _actions.asSharedFlow()

    private val actionManager: ActionManager<A> = object : ActionManager<A> {
        override fun send(action: A) {
            scope.launch { _actions.emit(action) }
        }
    }

    private suspend fun Flow<P>.collectAndReduce(state: MutableStateFlow<S>) {
        collect { state.value = it.reduce(state.value) }
    }

    override fun sendEvent(event: E) {
        scope.launch {
            onAction(actionManager, event)
        }
        scope.launch {
            onEvent(actionManager, event).collectAndReduce(_state)
        }
    }

    init {
        scope.launch {
            bindSources(actionManager).collectAndReduce(_state)
        }
    }
}