package com.badger.jetmviarticles.core.statedelegate

import com.badger.jetmviarticles.core.base.IBaseViewAction

interface ActionManager<T : IBaseViewAction> {
    fun send(action: T)
}