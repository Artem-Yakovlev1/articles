package com.badger.jetmviarticles.core.statedelegate

import com.badger.jetmviarticles.core.base.IBaseViewState

interface ReducibleState<S : IBaseViewState> {
    fun reduce(oldState: S): S
}