package com.badger.jetmviarticles.core.navigation

import kotlinx.coroutines.flow.SharedFlow

interface INavManager {
    val commands: SharedFlow<Command>

    fun navigate(command: Command)

    data class Command(
        val command: String
    )
}