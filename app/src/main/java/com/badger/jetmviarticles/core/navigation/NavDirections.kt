package com.badger.jetmviarticles.core.navigation

import androidx.lifecycle.SavedStateHandle
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavType
import androidx.navigation.navArgument

sealed class NavDirections {

    abstract val route: String

    abstract val arguments: List<NamedNavArgument>

    object Details : NavDirections() {

        private const val details = "details"
        private val articleIdArg = navArgument(name = "articleId") { type = NavType.StringType }

        override val route: String = "$details/{${articleIdArg.name}}"
        override val arguments: List<NamedNavArgument> = listOf(articleIdArg)

        fun getArticleId(state: SavedStateHandle) = state.get<String>(articleIdArg.name)

        fun createCommandWithArg(articleId: String): INavManager.Command {
            return INavManager.Command(command = "$details/$articleId")
        }
    }

    object Listing : NavDirections() {
        override val route: String = "listing"
        override val arguments: List<NamedNavArgument> = emptyList()

    }
}