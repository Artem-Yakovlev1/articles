package com.badger.jetmviarticles.core.base

import kotlinx.coroutines.flow.StateFlow

interface IBaseViewModel<S : IBaseViewState, E : IBaseViewEvent> {

    val state: StateFlow<S>

    fun onEvent(event: E)
}