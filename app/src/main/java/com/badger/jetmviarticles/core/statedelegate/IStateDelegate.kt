package com.badger.jetmviarticles.core.statedelegate

import com.badger.jetmviarticles.core.base.IBaseViewAction
import com.badger.jetmviarticles.core.base.IBaseViewEvent
import com.badger.jetmviarticles.core.base.IBaseViewState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

interface IStateDelegate<in E : IBaseViewEvent, out S : IBaseViewState, out A : IBaseViewAction> {
    val actions: Flow<A>
    val state: StateFlow<S>

    fun sendEvent(event: E)
}