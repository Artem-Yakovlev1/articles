package com.badger.jetmviarticles.core.navigation

import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow

class NavManager : INavManager {

    private val _commands = MutableSharedFlow<INavManager.Command>(extraBufferCapacity = 1)
    override var commands = _commands.asSharedFlow()

    override fun navigate(
        command: INavManager.Command
    ) {
        _commands.tryEmit(command)
    }
}