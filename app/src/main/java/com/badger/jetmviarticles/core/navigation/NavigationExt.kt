package com.badger.jetmviarticles.core.navigation

import androidx.compose.animation.AnimatedVisibilityScope
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.runtime.Composable
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavGraphBuilder
import com.google.accompanist.navigation.animation.composable


fun NavGraphBuilder.composableWithoutAnim(
    route: String,
    arguments: List<NamedNavArgument>,
    content: @Composable AnimatedVisibilityScope.(NavBackStackEntry) -> Unit
) = composable(
    route = route,
    arguments = arguments,
    enterTransition = { enterTransitionWithoutAnim() },
    exitTransition = { exitTransitionWithoutAnim() },
    content = content
)

private fun enterTransitionWithoutAnim() = fadeIn(animationSpec = tween(0))
private fun exitTransitionWithoutAnim() = fadeOut(animationSpec = tween(0))
