package com.badger.jetmviarticles.features.details.model

import com.badger.jetmviarticles.core.statedelegate.ReducibleState
import com.badger.jetmviarticles.data.entity.Article

sealed class DetailsReducibleState : ReducibleState<DetailsState> {

    data class ArticleLoaded(
        val article: Article
    ) : DetailsReducibleState() {
        override fun reduce(oldState: DetailsState): DetailsState {
            return DetailsState.ViewMode(article = article)
        }
    }

    data class OnDescriptionInputValueChanged(
        val value: String
    ) : DetailsReducibleState() {
        override fun reduce(oldState: DetailsState): DetailsState {
            return (oldState as? DetailsState.EditMode)
                ?.copy(descriptionInputValue = value) ?: oldState
        }
    }

    object SwitchToViewMode : DetailsReducibleState() {
        override fun reduce(oldState: DetailsState): DetailsState {
            return (oldState as? DetailsState.EditMode)
                ?.article?.let(DetailsState::ViewMode) ?: oldState
        }
    }

    object SwitchToEditMode : DetailsReducibleState() {
        override fun reduce(oldState: DetailsState): DetailsState {
            return (oldState as? DetailsState.ViewMode)
                ?.article?.let(DetailsState::EditMode) ?: oldState
        }

    }
}