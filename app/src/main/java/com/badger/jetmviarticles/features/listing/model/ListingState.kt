package com.badger.jetmviarticles.features.listing.model

import com.badger.jetmviarticles.core.base.IBaseViewState
import com.badger.jetmviarticles.data.entity.Article

sealed class ListingState : IBaseViewState {

    object Loading : ListingState()

    data class Loaded(
        val items: List<Article>
    ) : ListingState()
}