package com.badger.jetmviarticles.features.details.model

import com.badger.jetmviarticles.core.base.IBaseViewState
import com.badger.jetmviarticles.data.entity.Article

sealed class DetailsState : IBaseViewState {

    object Loading : DetailsState()

    data class ViewMode(
        val article: Article
    ) : DetailsState()

    data class EditMode(
        val article: Article,
        val descriptionInputValue: String = article.description,
    ) : DetailsState()
}

