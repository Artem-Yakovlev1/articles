package com.badger.jetmviarticles.features.details

import com.badger.jetmviarticles.core.base.IBaseViewModel
import com.badger.jetmviarticles.features.details.model.DetailsEvent
import com.badger.jetmviarticles.features.details.model.DetailsState

interface IDetailsViewModel : IBaseViewModel<DetailsState, DetailsEvent>