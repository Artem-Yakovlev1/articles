package com.badger.jetmviarticles.features.listing

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.badger.jetmviarticles.data.entity.Article
import com.badger.jetmviarticles.features.listing.model.ListingEvent
import com.badger.jetmviarticles.features.listing.model.ListingState

@Composable
fun ListingScreen(
    viewModel: IListingViewModel
) {
    val state by viewModel.state.collectAsState()
    RenderState(state = state, onEvent = viewModel::onEvent)
}

@Composable
private fun RenderState(
    state: ListingState,
    onEvent: (ListingEvent) -> Unit
) {
    when (state) {
        is ListingState.Loading -> LoadingState()
        is ListingState.Loaded -> LoadedState(
            state = state,
            onClick = { articleId -> onEvent(ListingEvent.NavigateToDetails(articleId)) }
        )
    }
}

@Composable
private fun LoadingState() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator()
    }
}

@Composable
private fun LoadedState(
    state: ListingState.Loaded,
    onClick: (String) -> Unit
) {
    LazyColumn {
        items(state.items) { article ->
            ListingItem(
                article = article,
                onClick = { onClick(article.id) }
            )
        }
    }
}

@Composable
private fun ListingItem(
    article: Article,
    onClick: () -> Unit
) {
    Card(
        modifier = Modifier
            .wrapContentHeight()
            .fillMaxWidth()
            .clickable { onClick() }
            .padding(horizontal = 8.dp, vertical = 4.dp),
        shape = RoundedCornerShape(8.dp),
    ) {
        Column(
            modifier = Modifier.padding(
                horizontal = 16.dp, vertical = 8.dp
            ),
            horizontalAlignment = Alignment.Start
        ) {
            Text(
                text = article.title,
                style = MaterialTheme.typography.h6,
                maxLines = 1
            )
            Text(
                text = article.description,
                style = MaterialTheme.typography.subtitle1,
                maxLines = 1
            )
        }
    }
}