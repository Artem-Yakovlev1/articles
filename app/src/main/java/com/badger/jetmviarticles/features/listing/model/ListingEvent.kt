package com.badger.jetmviarticles.features.listing.model

import com.badger.jetmviarticles.core.base.IBaseViewEvent

sealed class ListingEvent : IBaseViewEvent {
    data class NavigateToDetails(val articleId: String) : ListingEvent()
}