package com.badger.jetmviarticles.features.listing

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.badger.jetmviarticles.core.statedelegate.IStateDelegate
import com.badger.jetmviarticles.core.statedelegate.StateDelegate
import com.badger.jetmviarticles.core.navigation.INavManager
import com.badger.jetmviarticles.core.navigation.NavDirections
import com.badger.jetmviarticles.data.source.IArticlesSource
import com.badger.jetmviarticles.features.listing.model.ListingEvent
import com.badger.jetmviarticles.features.listing.model.ListingReducibleState
import com.badger.jetmviarticles.features.listing.model.ListingState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ListingViewModel @Inject constructor(
    private val navManager: INavManager,
    private val articlesSource: IArticlesSource
) : ViewModel(), IListingViewModel {

    private val stateDelegate: IStateDelegate<ListingEvent, ListingState, Nothing> =
        StateDelegate(
            scope = viewModelScope,
            initialState = ListingState.Loading,
            bindSources = { articlesSource.getArticles().map(ListingReducibleState::ItemsLoaded) },
            onAction = { _, event -> processAsAction(event) }
        )

    override val state: StateFlow<ListingState> = stateDelegate.state

    override fun onEvent(event: ListingEvent) = stateDelegate.sendEvent(event)

    private fun processAsAction(event: ListingEvent) {
        when (event) {
            is ListingEvent.NavigateToDetails -> viewModelScope.launch {
                navManager.navigate(
                    NavDirections.Details.createCommandWithArg(
                        articleId = event.articleId
                    )
                )
            }
        }
    }
}