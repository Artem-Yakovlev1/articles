package com.badger.jetmviarticles.features.details.model

import com.badger.jetmviarticles.core.base.IBaseViewEvent

sealed class DetailsEvent : IBaseViewEvent {
    data class OnDescriptionInputValueChanged(val value: String) : DetailsEvent()
    object SwitchToEditMode : DetailsEvent()
    data class SaveDescription(val articleId: String, val description: String) : DetailsEvent()
}