package com.badger.jetmviarticles.features.details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.badger.jetmviarticles.core.navigation.NavDirections
import com.badger.jetmviarticles.core.statedelegate.IStateDelegate
import com.badger.jetmviarticles.core.statedelegate.StateDelegate
import com.badger.jetmviarticles.data.source.IArticlesSource
import com.badger.jetmviarticles.features.details.model.DetailsEvent
import com.badger.jetmviarticles.features.details.model.DetailsReducibleState
import com.badger.jetmviarticles.features.details.model.DetailsState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import javax.inject.Inject

@HiltViewModel
class DetailsViewModel @Inject constructor(
    private val articlesSource: IArticlesSource,
    savedStateHandle: SavedStateHandle
) : ViewModel(), IDetailsViewModel {

    private val stateDelegate: IStateDelegate<DetailsEvent, DetailsState, Nothing> =
        StateDelegate(
            scope = viewModelScope,
            initialState = DetailsState.Loading,
            bindSources = {
                val articleId = NavDirections.Details.getArticleId(savedStateHandle).orEmpty()
                articlesSource.getArticleById(articleId)
                    .filterNotNull().map(DetailsReducibleState::ArticleLoaded)
            },
            onEvent = { _, event -> processAsEvent(event) }
        )

    override val state: StateFlow<DetailsState> = stateDelegate.state

    override fun onEvent(event: DetailsEvent) = stateDelegate.sendEvent(event = event)

    private suspend fun processAsEvent(event: DetailsEvent): Flow<DetailsReducibleState> {
        return when (event) {
            is DetailsEvent.OnDescriptionInputValueChanged -> {
                flowOf(DetailsReducibleState.OnDescriptionInputValueChanged(event.value))
            }
            is DetailsEvent.SwitchToEditMode -> {
                flowOf(DetailsReducibleState.SwitchToEditMode)
            }
            is DetailsEvent.SaveDescription -> {
                updateArticleDescription(
                    articleId = event.articleId,
                    description = event.description
                )
                flowOf(DetailsReducibleState.SwitchToViewMode)
            }
        }
    }

    private suspend fun updateArticleDescription(articleId: String, description: String) {
        articlesSource.changeArticleDescription(
            id = articleId,
            description = description
        )
    }
}