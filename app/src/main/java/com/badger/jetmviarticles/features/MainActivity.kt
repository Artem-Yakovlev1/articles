package com.badger.jetmviarticles.features

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import com.badger.jetmviarticles.core.navigation.INavManager
import com.badger.jetmviarticles.core.navigation.NavDirections
import com.badger.jetmviarticles.core.navigation.composableWithoutAnim
import com.badger.jetmviarticles.features.details.DetailsScreen
import com.badger.jetmviarticles.features.details.DetailsViewModel
import com.badger.jetmviarticles.features.details.IDetailsViewModel
import com.badger.jetmviarticles.features.listing.IListingViewModel
import com.badger.jetmviarticles.features.listing.ListingScreen
import com.badger.jetmviarticles.features.listing.ListingViewModel
import com.badger.jetmviarticles.ui.theme.JetMviArticlesTheme
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @Inject
    lateinit var navManager: INavManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            JetMviArticlesTheme {
                AppScreen(navManager = navManager)
            }
        }
    }
}

private const val NAVIGATION_KEY = "navigation"
private const val NAV_TIMEOUT = 100L

@Composable
fun AppScreen(navManager: INavManager) {
    val navController = rememberAnimatedNavController()

    LaunchedEffect(NAVIGATION_KEY) {
        navManager.commands.debounce(NAV_TIMEOUT).onEach { destination ->
            navController.navigate(destination.command)
        }.launchIn(this)
    }

    AnimatedNavHost(
        modifier = Modifier.fillMaxSize(),
        navController = navController,
        startDestination = NavDirections.Listing.route
    ) {

        composableWithoutAnim(
            route = NavDirections.Listing.route,
            arguments = NavDirections.Listing.arguments
        ) {
            val viewModel: IListingViewModel = hiltViewModel<ListingViewModel>()
            ListingScreen(
                viewModel = viewModel
            )
        }

        composableWithoutAnim(
            route = NavDirections.Details.route,
            arguments = NavDirections.Details.arguments
        ) {
            val viewModel: IDetailsViewModel = hiltViewModel<DetailsViewModel>()
            DetailsScreen(
                viewModel = viewModel
            )
        }
    }
}