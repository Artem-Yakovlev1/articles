package com.badger.jetmviarticles.features.details

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.badger.jetmviarticles.R
import com.badger.jetmviarticles.data.entity.Article
import com.badger.jetmviarticles.features.details.model.DetailsEvent
import com.badger.jetmviarticles.features.details.model.DetailsState

@Composable
fun DetailsScreen(
    viewModel: IDetailsViewModel
) {
    val state by viewModel.state.collectAsState()
    RenderState(state = state, onEvent = viewModel::onEvent)
}

@Composable
private fun RenderState(
    state: DetailsState,
    onEvent: (DetailsEvent) -> Unit
) {
    when (state) {
        is DetailsState.EditMode -> EditModeState(
            state = state,
            onInputChanged = { input ->
                val event = DetailsEvent.OnDescriptionInputValueChanged(
                    value = input
                )
                onEvent(event)
            },
            onSaveClick = { description ->
                val event = DetailsEvent.SaveDescription(
                    articleId = state.article.id,
                    description = description
                )
                onEvent(event)
            }
        )
        is DetailsState.ViewMode -> ViewModeState(
            state = state,
            onClick = { onEvent(DetailsEvent.SwitchToEditMode) }
        )
        is DetailsState.Loading -> LoadingState()
    }
}

@Composable
private fun LoadingState() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator()
    }
}

@Composable
private fun EditModeState(
    state: DetailsState.EditMode,
    onInputChanged: (String) -> Unit,
    onSaveClick: (String) -> Unit
) {
    DescriptionPattern(
        article = state.article
    ) {
        ArticleEditableDescription(
            currentDetails = state.descriptionInputValue,
            onInputChanged = onInputChanged,
            onChangeModeClick = { onSaveClick(state.descriptionInputValue) }
        )
    }
}

@Composable
private fun ViewModeState(
    state: DetailsState.ViewMode,
    onClick: () -> Unit
) {
    DescriptionPattern(
        article = state.article
    ) {
        ArticleStaticDescription(
            description = state.article.description,
            onChangeModeClick = onClick
        )
    }
}


@Composable
private fun ArticleHeader(
    modifier: Modifier = Modifier,
    article: Article
) {
    Card(
        modifier = modifier,
        shape = RoundedCornerShape(8.dp),
    ) {
        Column(
            modifier = Modifier.padding(
                horizontal = 16.dp, vertical = 8.dp
            ),
            horizontalAlignment = Alignment.Start
        ) {
            Text(
                text = article.title,
                style = MaterialTheme.typography.h6
            )
            Text(
                text = article.author,
                style = MaterialTheme.typography.subtitle2
            )
        }
    }
}

@Composable
private fun DescriptionPattern(
    article: Article,
    content: @Composable ColumnScope.() -> Unit

) {
    Column(
        modifier = Modifier
            .padding(horizontal = 16.dp, vertical = 8.dp)
    ) {
        ArticleHeader(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            article = article
        )
        Spacer(modifier = Modifier.height(8.dp))

        Card(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight(),
            shape = RoundedCornerShape(8.dp),
        ) {
            Column(
                modifier = Modifier.padding(
                    horizontal = 16.dp, vertical = 8.dp
                ),
                horizontalAlignment = Alignment.Start,
                content = content
            )
        }
    }
}

private const val DESCRIPTION_MAX_LINES = 5

@Composable
private fun ArticleStaticDescription(
    description: String,
    onChangeModeClick: () -> Unit
) {
    Text(
        text = description,
        style = MaterialTheme.typography.body1,
        maxLines = DESCRIPTION_MAX_LINES
    )
    Button(onClick = { onChangeModeClick() }) {
        Text(text = stringResource(id = R.string.details_edit))
    }

}

@Composable
private fun ArticleEditableDescription(
    currentDetails: String,
    onInputChanged: (String) -> Unit,
    onChangeModeClick: () -> Unit
) {
    TextField(
        value = currentDetails,
        onValueChange = onInputChanged,
        maxLines = DESCRIPTION_MAX_LINES
    )
    Button(onClick = { onChangeModeClick() }) {
        Text(text = stringResource(id = R.string.details_save))
    }

}