package com.badger.jetmviarticles.features.listing

import com.badger.jetmviarticles.core.base.IBaseViewModel
import com.badger.jetmviarticles.features.listing.model.ListingEvent
import com.badger.jetmviarticles.features.listing.model.ListingState

interface IListingViewModel : IBaseViewModel<ListingState, ListingEvent>
