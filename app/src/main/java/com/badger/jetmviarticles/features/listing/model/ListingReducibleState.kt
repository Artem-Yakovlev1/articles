package com.badger.jetmviarticles.features.listing.model

import com.badger.jetmviarticles.core.statedelegate.ReducibleState
import com.badger.jetmviarticles.data.entity.Article

sealed class ListingReducibleState : ReducibleState<ListingState> {

    data class ItemsLoaded(
        val items: List<Article>
    ) : ListingReducibleState() {
        override fun reduce(oldState: ListingState): ListingState {
            return ListingState.Loaded(items)
        }
    }
}