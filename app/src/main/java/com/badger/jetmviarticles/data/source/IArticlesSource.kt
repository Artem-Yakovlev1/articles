package com.badger.jetmviarticles.data.source

import com.badger.jetmviarticles.data.entity.Article
import kotlinx.coroutines.flow.Flow

interface IArticlesSource {

    fun getArticles(): Flow<List<Article>>

    fun getArticleById(id: String): Flow<Article?>

    suspend fun changeArticleDescription(id: String, description: String)
}