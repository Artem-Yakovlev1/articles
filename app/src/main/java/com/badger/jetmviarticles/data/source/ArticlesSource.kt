package com.badger.jetmviarticles.data.source

import com.badger.jetmviarticles.data.entity.Article
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class ArticlesSource @Inject constructor() : IArticlesSource {

    private val articles = MutableStateFlow(createInitialArticles())

    override fun getArticles(): Flow<List<Article>> {
        return articles
    }

    override fun getArticleById(id: String): Flow<Article?> {
        return articles.map { list -> list.findById(id) }
    }

    private fun List<Article>.findById(id: String): Article? {
        return find { item -> item.id == id }
    }

    override suspend fun changeArticleDescription(id: String, description: String) {
        articles.value = articles.value.map { article ->
            if (article.id == id) article.copy(description = description) else article
        }
    }

    companion object {
        private fun createInitialArticles() = List(size = 12, init = Article::createMock)
    }

}