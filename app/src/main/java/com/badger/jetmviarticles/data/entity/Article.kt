package com.badger.jetmviarticles.data.entity

data class Article(
    val id: String,
    val title: String,
    val author: String,
    val description: String
) {
    companion object {
        fun createMock(id: Int) = Article(
            id = id.toString(),
            title = "Заголовок $id",
            author = "Автор",
            description = "Полное описание, доступное только в Details"
        )
    }
}