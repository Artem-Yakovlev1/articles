package com.badger.jetmviarticles.di

import com.badger.jetmviarticles.core.navigation.INavManager
import com.badger.jetmviarticles.core.navigation.NavManager
import com.badger.jetmviarticles.data.source.ArticlesSource
import com.badger.jetmviarticles.data.source.IArticlesSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.DelicateCoroutinesApi
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
@DelicateCoroutinesApi
class SourceModule {

    @Singleton
    @Provides
    fun providesNavigationManager(): INavManager = NavManager()

    @Singleton
    @Provides
    fun providesArticlesSource(): IArticlesSource = ArticlesSource()
}